![New China Emblem](../images/EAJHQx4UcAIR8.jpg "New China Emblem")

Image designed by: @badiucao - https://twitter.com/badiucao

Some online safety ideas to help protect people in Hong Kong and other places around the world. Please visit and add to the wiki here: 
https://gitlab.com/medelingforeigner/online-off-the-record/wikis/Online-Off-Record
So far it covers instant messaging, social media, advice for people in danger and groups with technical resources that want to run their own systems.